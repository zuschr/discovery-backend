package de.ebertsoftware.discovery.controller;

import de.ebertsoftware.discovery.domain.song.SongEntity;
import de.ebertsoftware.discovery.domain.song.command.FetchSongsFromRemoteCommand;
import de.ebertsoftware.discovery.domain.song.model.SongListModel;
import de.ebertsoftware.discovery.domain.song.model.SongModel;
import de.ebertsoftware.discovery.domain.song.repository.SongRepository;
import de.ebertsoftware.discovery.framework.CommandHandlerDispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api/v1/songs", produces = MediaType.APPLICATION_JSON_VALUE)
public class SongListController {

    @Autowired
    CommandHandlerDispatcher<List<SongEntity>> songsCommandHandlerDispatcher;

    @Autowired
    SongRepository songRepository;

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public Resource<SongListModel> searchSongs(@RequestParam String param) {
        List<SongEntity> songs = songsCommandHandlerDispatcher.dispatch(new FetchSongsFromRemoteCommand(param));
        return assembleResource(songs, param);
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public Resource<SongListModel> getAllSongs() {
        List<SongEntity> songs = newArrayList(songRepository.findAll());

        return assembleResource(songs, null);
    }

    @RequestMapping(path = "/random", method = RequestMethod.GET)
    public Resource<SongListModel> fetchRandom() {
        List<SongEntity> songs = songsCommandHandlerDispatcher.dispatch(new FetchSongsFromRemoteCommand());
        return assembleResource(songs, null);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Resource<SongModel> getSong(@PathVariable String id) {
        SongEntity song = songRepository.findById(id).orElseThrow(RuntimeException::new);
        return assembleResource(song);
    }

    private Resource<SongModel> assembleResource(SongEntity songEntity) {
        SongModel song = new SongModel(songEntity);
        Resource<SongModel> resource = new Resource<>(song);
        resource.add(linkTo(methodOn(SongListController.class).getSong(song.getId())).withSelfRel());
        return resource;
    }

    private Resource<SongListModel> assembleResource(List<SongEntity> songs, String searchterm) {
        List<Resource<SongModel>> songResources = songs.stream().map(this::assembleResource).collect(Collectors.toList());
        Resource<SongListModel> resource = new Resource<>(new SongListModel(songResources));
        if (searchterm != null) {
            resource.add(linkTo(methodOn(SongListController.class).searchSongs(searchterm)).withSelfRel());
        }
        resource.add(linkTo(methodOn(SongListController.class).getAllSongs()).withRel("all"));
        return resource;
    }
}
