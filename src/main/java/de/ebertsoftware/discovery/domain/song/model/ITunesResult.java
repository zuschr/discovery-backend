package de.ebertsoftware.discovery.domain.song.model;

public class ITunesResult {

    private String artworkUrl100;

    private String country;

    private String copyright;

    private String collectionViewUrl;

    private String previewUrl;

    private String releaseDate;

    private String description;

    private String artistId;

    private String collectionPrice;

    private String primaryGenreName;

    private String collectionName;

    private String artistViewUrl;

    private String collectionExplicitness;

    private String trackCount;

    private String artworkUrl60;

    private String wrapperType;

    private String artistName;

    private String currency;

    private String collectionId;

    private String collectionCensoredName;

    public String getArtworkUrl100 ()
    {
        return artworkUrl100;
    }

    public void setArtworkUrl100 (String artworkUrl100)
    {
        this.artworkUrl100 = artworkUrl100;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getCopyright ()
    {
        return copyright;
    }

    public void setCopyright (String copyright)
    {
        this.copyright = copyright;
    }

    public String getCollectionViewUrl ()
    {
        return collectionViewUrl;
    }

    public void setCollectionViewUrl (String collectionViewUrl)
    {
        this.collectionViewUrl = collectionViewUrl;
    }

    public String getPreviewUrl ()
    {
        return previewUrl;
    }

    public void setPreviewUrl (String previewUrl)
    {
        this.previewUrl = previewUrl;
    }

    public String getReleaseDate ()
    {
        return releaseDate;
    }

    public void setReleaseDate (String releaseDate)
    {
        this.releaseDate = releaseDate;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getArtistId ()
    {
        return artistId;
    }

    public void setArtistId (String artistId)
    {
        this.artistId = artistId;
    }

    public String getCollectionPrice ()
    {
        return collectionPrice;
    }

    public void setCollectionPrice (String collectionPrice)
    {
        this.collectionPrice = collectionPrice;
    }

    public String getPrimaryGenreName ()
    {
        return primaryGenreName;
    }

    public void setPrimaryGenreName (String primaryGenreName)
    {
        this.primaryGenreName = primaryGenreName;
    }

    public String getCollectionName ()
    {
        return collectionName;
    }

    public void setCollectionName (String collectionName)
    {
        this.collectionName = collectionName;
    }

    public String getArtistViewUrl ()
    {
        return artistViewUrl;
    }

    public void setArtistViewUrl (String artistViewUrl)
    {
        this.artistViewUrl = artistViewUrl;
    }

    public String getCollectionExplicitness ()
    {
        return collectionExplicitness;
    }

    public void setCollectionExplicitness (String collectionExplicitness)
    {
        this.collectionExplicitness = collectionExplicitness;
    }

    public String getTrackCount ()
    {
        return trackCount;
    }

    public void setTrackCount (String trackCount)
    {
        this.trackCount = trackCount;
    }

    public String getArtworkUrl60 ()
    {
        return artworkUrl60;
    }

    public void setArtworkUrl60 (String artworkUrl60)
    {
        this.artworkUrl60 = artworkUrl60;
    }

    public String getWrapperType ()
    {
        return wrapperType;
    }

    public void setWrapperType (String wrapperType)
    {
        this.wrapperType = wrapperType;
    }

    public String getArtistName ()
    {
        return artistName;
    }

    public void setArtistName (String artistName)
    {
        this.artistName = artistName;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public String getCollectionId ()
    {
        return collectionId;
    }

    public void setCollectionId (String collectionId)
    {
        this.collectionId = collectionId;
    }

    public String getCollectionCensoredName ()
    {
        return collectionCensoredName;
    }

    public void setCollectionCensoredName (String collectionCensoredName)
    {
        this.collectionCensoredName = collectionCensoredName;
    }
}
