package de.ebertsoftware.discovery.domain.song.handler;

import org.ajbrown.namemachine.NameGenerator;
import org.springframework.stereotype.Component;

@Component
public class RandomNameGenerator {

    private final NameGenerator nameGenerator;

    public RandomNameGenerator() {
        this.nameGenerator = new NameGenerator();
    }

    public String generateName() {
        return nameGenerator.generateName().toString();
    }
}
