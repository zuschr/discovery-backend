package de.ebertsoftware.discovery.domain.song.handler;

import de.ebertsoftware.discovery.domain.song.SongPlatformInformation;
import de.ebertsoftware.discovery.domain.song.model.ITunesResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static de.ebertsoftware.discovery.domain.song.PlatformType.ITUNES;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

@Component
public class ITunesSongFetcher implements SongFetcher {

    private static final String ITUNES_API_URL = "https://itunes.apple.com/search?term=%s&media=music";

    private final Logger logger = LoggerFactory.getLogger(ITunesSongFetcher.class);
    private final RestTemplate restTemplate;

    public ITunesSongFetcher() {
        restTemplate = new RestTemplate();
        for (HttpMessageConverter<?> myConverter : restTemplate.getMessageConverters ()) {
            if (myConverter instanceof MappingJackson2HttpMessageConverter) {
                List<MediaType> myMediaTypes = new ArrayList<>(myConverter.getSupportedMediaTypes());
                myMediaTypes.add (MediaType.parseMediaType ("text/javascript; charset=utf-8"));
                ((MappingJackson2HttpMessageConverter) myConverter).setSupportedMediaTypes (myMediaTypes);
            }
        }
    }

    public List<SongPlatformInformation> fetchSongs(String searchTerm) {
        try {
            String encodedSearchTerm = URLEncoder.encode(searchTerm, StandardCharsets.UTF_8.toString());
            ITunesResults results = restTemplate.getForObject(format(ITUNES_API_URL, encodedSearchTerm), ITunesResults.class);
            return convertITunesResultsToSongs(results);
        } catch (UnsupportedEncodingException e) {
            logger.error("Error while contacting remote API: ", e);
        }
        return emptyList();
    }

    private List<SongPlatformInformation> convertITunesResultsToSongs(ITunesResults results) {
        if (results == null) return emptyList();

        return Arrays.stream(results.getResults()).map((result) -> new SongPlatformInformation(
                ITUNES,
                result.getCollectionId(),
                result.getArtistId(),
                result.getCollectionName(),
                result.getArtistName(),
                result.getPreviewUrl(),
                result.getArtworkUrl100(),
                singletonList(result.getPrimaryGenreName())
        )).collect(Collectors.toList());
    }
}
