package de.ebertsoftware.discovery.domain.song.repository;

import de.ebertsoftware.discovery.domain.song.SongEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SongRepository extends MongoRepository<SongEntity, String> {

}
