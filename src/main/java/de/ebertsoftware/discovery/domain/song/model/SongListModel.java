package de.ebertsoftware.discovery.domain.song.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.Resource;

import java.util.List;

public class SongListModel {

    private List<Resource<SongModel>> songs;

    public SongListModel(@JsonProperty("songs") List<Resource<SongModel>> songs) {
        this.songs = songs;
    }

    public List<Resource<SongModel>> getSongs() {
        return songs;
    }

    public void setSongs(List<Resource<SongModel>> songs) {
        this.songs = songs;
    }
}
