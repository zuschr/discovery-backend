package de.ebertsoftware.discovery.domain.song.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.ebertsoftware.discovery.domain.song.PlatformType;
import de.ebertsoftware.discovery.domain.song.SongEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static de.ebertsoftware.discovery.domain.song.PlatformType.ITUNES;

public class SongModel {
    private String id;
    private String artworkUrl;
    private String previewUrl;
    private String artistName;
    private String songName;
    private List<PlatformType> platforms;

    public SongModel(
            @JsonProperty String id,
            @JsonProperty String artworkUrl,
            @JsonProperty String previewUrl,
            @JsonProperty String artistName,
            @JsonProperty String songName,
            @JsonProperty List<PlatformType> platforms) {

        this.id = id;
        this.artworkUrl = artworkUrl;
        this.previewUrl = previewUrl;
        this.artistName = artistName;
        this.songName = songName;
        this.platforms = platforms;
    }

    public SongModel(SongEntity songEntity) {
        this.id = songEntity.getId();
        this.platforms = new ArrayList<>();

       songEntity.getPlatformInformation().stream().filter((info) -> ITUNES == info.getPlatform()).findFirst().ifPresent(songPlatformInformation -> {
           this.artworkUrl = songPlatformInformation.getArtworkUrl();
           this.previewUrl = songPlatformInformation.getPreviewUrl();
           this.artistName = songPlatformInformation.getArtistName();
           this.songName = songPlatformInformation.getSongName();
       });

       songEntity.getPlatformInformation().forEach(songPlatformInformation -> this.platforms.add(songPlatformInformation.getPlatform()));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public List<PlatformType> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<PlatformType> platforms) {
        this.platforms = platforms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongModel songModel = (SongModel) o;
        return Objects.equals(id, songModel.id) &&
                Objects.equals(artworkUrl, songModel.artworkUrl) &&
                Objects.equals(previewUrl, songModel.previewUrl) &&
                Objects.equals(artistName, songModel.artistName) &&
                Objects.equals(songName, songModel.songName) &&
                Objects.equals(platforms, songModel.platforms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, artworkUrl, previewUrl, artistName, songName, platforms);
    }
}
