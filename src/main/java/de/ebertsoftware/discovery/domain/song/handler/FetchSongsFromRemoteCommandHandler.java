package de.ebertsoftware.discovery.domain.song.handler;

import de.ebertsoftware.discovery.domain.song.SongEntity;
import de.ebertsoftware.discovery.domain.song.SongPlatformInformation;
import de.ebertsoftware.discovery.domain.song.command.FetchSongsFromRemoteCommand;
import de.ebertsoftware.discovery.domain.song.repository.SongRepository;
import de.ebertsoftware.discovery.framework.CommandHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;

@Component
public class FetchSongsFromRemoteCommandHandler implements CommandHandler<FetchSongsFromRemoteCommand, List<SongEntity>> {

    private final Logger logger = LoggerFactory.getLogger(FetchSongsFromRemoteCommandHandler.class);

    private static final int RETRIES = 20;

    private final SongRepository songRepository;
    private final RandomNameGenerator nameGenerator;
    private final List<SongFetcher> songFetchers;

    public FetchSongsFromRemoteCommandHandler(@Autowired SongRepository songRepository,
                                              @Autowired List<SongFetcher> songFetchers,
                                              @Autowired RandomNameGenerator randomNameGenerator) {
        this.songRepository = songRepository;
        this.nameGenerator = randomNameGenerator;
        this.songFetchers = songFetchers;
    }

    @Override
    public List<SongEntity> handle(FetchSongsFromRemoteCommand cmd) {
        checkNotNull(cmd, "Command should not be null");

        String searchTerm = cmd.isRandom() ? getRandomName() : cmd.getSearchTerm();

        for(int run=0; run < RETRIES; run++) {
            List<SongEntity> songs = fetchResults(searchTerm);

            if (songs.size() > 0) {
                songRepository.saveAll(songs);
                return songs;
            } else if (cmd.isRandom()) {
                logger.debug("Could not get any result for search term [{}] in run [{}]", searchTerm, run);
                searchTerm = getRandomName();
            }
        }
        return emptyList();
    }

    private String getRandomName() {
        return nameGenerator.generateName();
    }

    private List<SongEntity> fetchResults(String searchTerm) {
        if (songFetchers.size() > 0 ) {
            SongFetcher primarySongFetcher = songFetchers.get(0);
            return primarySongFetcher
                    .fetchSongs(searchTerm)
                    .stream()
                    .map(primaryPlatformInformation -> songFetchers
                            .stream()
                            .map(songFetcher -> songFetcher.fetchSongs(getSearchTermFromSongPlatformInformation(primaryPlatformInformation)))
                            .filter(list -> list != null && !list.isEmpty())
                            .map(list -> findBestMatchForSongPlatformInformation(primaryPlatformInformation, list))
                            .collect(Collectors.toList()))
                    .filter(list -> list.size() == songFetchers.size())
                    .map((platformInformationList -> new SongEntity(UUID.randomUUID().toString(), platformInformationList)))
                    .collect(Collectors.toList());
        }
        return emptyList();

    }

    private String getSearchTermFromSongPlatformInformation(SongPlatformInformation songPlatformInformation) {
        //TODO: Improve
        return songPlatformInformation.getSongName().concat(" ").concat(songPlatformInformation.getArtistName());
    }

    private SongPlatformInformation findBestMatchForSongPlatformInformation(SongPlatformInformation reference, List<SongPlatformInformation> valuesToCheck) {
        //TODO: Improve
        return valuesToCheck.get(0);
    }
}
