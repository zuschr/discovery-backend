package de.ebertsoftware.discovery.domain.song.model;

public class ITunesResults {

    private String resultCount;

    private ITunesResult[] results;

    public String getResultCount ()
    {
        return resultCount;
    }

    public void setResultCount (String resultCount)
    {
        this.resultCount = resultCount;
    }

    public ITunesResult[] getResults ()
    {
        return results;
    }

    public void setResults (ITunesResult[] results)
    {
        this.results = results;
    }
}
