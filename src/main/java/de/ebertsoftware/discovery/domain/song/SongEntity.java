package de.ebertsoftware.discovery.domain.song;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

@Document(collection = "songs")
public class SongEntity {

    @Id
    private String id;
    private List<SongPlatformInformation> platformInformation;

    public SongEntity(
            @JsonProperty String id,
            @JsonProperty List<SongPlatformInformation> platformInformation) {

        this.id = id;
        this.platformInformation = platformInformation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPlatformInformation(List<SongPlatformInformation> platforminformation) {
        this.platformInformation = platforminformation;
    }

    public List<SongPlatformInformation> getPlatformInformation() {
        return platformInformation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongEntity that = (SongEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(platformInformation, that.platformInformation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, platformInformation);
    }
}
