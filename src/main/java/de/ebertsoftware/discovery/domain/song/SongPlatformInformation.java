package de.ebertsoftware.discovery.domain.song;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class SongPlatformInformation {

    private PlatformType platform;
    private String songId;
    private String artistId;
    private String songName;
    private String artistName;
    private String previewUrl;
    private String artworkUrl;
    private List<String> genres;

    public SongPlatformInformation(
            @JsonProperty PlatformType platform,
            @JsonProperty String songId,
            @JsonProperty String artistId,
            @JsonProperty String songName,
            @JsonProperty String artistName,
            @JsonProperty String previewUrl,
            @JsonProperty String artworkUrl,
            @JsonProperty List<String> genres) {

        this.platform = platform;
        this.songId = songId;
        this.artistId = artistId;
        this.songName = songName;
        this.artistName = artistName;
        this.previewUrl = previewUrl;
        this.artworkUrl = artworkUrl;
        this.genres = genres;
    }

    public PlatformType getPlatform() {
        return platform;
    }

    public void setPlatform(PlatformType platform) {
        this.platform = platform;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongPlatformInformation that = (SongPlatformInformation) o;
        return Objects.equals(platform, that.platform) &&
                Objects.equals(songId, that.songId) &&
                Objects.equals(artistId, that.artistId) &&
                Objects.equals(songName, that.songName) &&
                Objects.equals(artistName, that.artistName) &&
                Objects.equals(previewUrl, that.previewUrl) &&
                Objects.equals(artworkUrl, that.artworkUrl) &&
                Objects.equals(genres, that.genres);
    }

    @Override
    public int hashCode() {
        return Objects.hash(platform, songId, artistId, songName, artistName, previewUrl, artworkUrl, genres);
    }
}
