package de.ebertsoftware.discovery.domain.song.handler;

import de.ebertsoftware.discovery.domain.song.SongPlatformInformation;

import java.util.List;

public interface SongFetcher {
    List<SongPlatformInformation> fetchSongs(String searchTerm);
}
