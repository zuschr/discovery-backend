package de.ebertsoftware.discovery.domain.song.command;

import de.ebertsoftware.discovery.framework.Command;

import static com.google.common.base.Preconditions.checkArgument;

public class FetchSongsFromRemoteCommand implements Command {

    private final String searchTerm;
    private final boolean random;

    public FetchSongsFromRemoteCommand(String searchTerm) {
        checkArgument(searchTerm != null && !searchTerm.isEmpty(), "searchTerm should not be null or empty");

        this.searchTerm = searchTerm;
        this.random = false;
    }

    public FetchSongsFromRemoteCommand() {
        this.searchTerm = null;
        this.random = true;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public boolean isRandom() {
        return random;
    }
}
