package de.ebertsoftware.discovery.domain.song;

public enum PlatformType {
    ITUNES,
    SPOTIFY
}
