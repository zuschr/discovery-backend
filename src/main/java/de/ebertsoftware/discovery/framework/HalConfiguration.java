package de.ebertsoftware.discovery.framework;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.Collections;
import java.util.List;

@Configuration
public class HalConfiguration extends WebMvcConfigurationSupport {

    private final ObjectMapper objectMapper;

    @Autowired
    public HalConfiguration(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private HttpMessageConverter customConverters() {

        final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = objectMapper.copy();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.registerModule(new Jackson2HalModule());
        converter.setObjectMapper(mapper);

        return converter;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(customConverters());
        super.addDefaultHttpMessageConverters(converters);
    }
}
