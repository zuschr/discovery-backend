package de.ebertsoftware.discovery.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class CommandHandlerDispatcher<R> {

    private static Logger logger = LoggerFactory.getLogger(CommandHandlerDispatcher.class);

    public interface HandlersProvider<R> {
        CommandHandler<Command, R> getHandler(Command command);
    }

    @Autowired
    private HandlersProvider handlerProvider;

    public R dispatch(Command command) {
        CommandHandler<Command, R> handler = handlerProvider.getHandler(command);
        logger.info("Dispatching command [{}] to CommandHandler [{}]",command.getClass().getName(),handler.getClass().getName());
        return handler.handle(command);

    }
}
