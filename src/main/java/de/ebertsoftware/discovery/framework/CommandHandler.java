package de.ebertsoftware.discovery.framework;

public interface CommandHandler<C, R> {
    R handle(C cmd);
}
