package de.ebertsoftware.discovery.domain.song.handler;

import de.ebertsoftware.discovery.domain.song.SongEntity;
import de.ebertsoftware.discovery.domain.song.SongPlatformInformation;
import de.ebertsoftware.discovery.domain.song.command.FetchSongsFromRemoteCommand;
import de.ebertsoftware.discovery.domain.song.repository.SongRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static de.ebertsoftware.discovery.SongEntityFixtures.aSongPlatformInformation;
import static de.ebertsoftware.discovery.SongEntityFixtures.aSongPlatformInformationWithType;
import static de.ebertsoftware.discovery.domain.song.PlatformType.ITUNES;
import static de.ebertsoftware.discovery.domain.song.PlatformType.SPOTIFY;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class FetchSongsFromRemoteCommandHandlerTest {

    @Mock
    private SongRepository songRepository;

    @Mock
    private ITunesSongFetcher iTunesSongFetcher;

    @Mock
    private RandomNameGenerator randomNameGenerator;

    @Mock
    private SpotifyPlatformFetcher spotifyPlatformFetcher;

    private FetchSongsFromRemoteCommandHandler sut;

    @Before
    public void setUp() {
        initMocks(this);

        when(randomNameGenerator.generateName()).thenReturn("randomName");

        sut = new FetchSongsFromRemoteCommandHandler(songRepository, singletonList(iTunesSongFetcher), randomNameGenerator);
    }

    @Test
    public void thatExceptionIsThrownIfCommandIsNull() {

        Throwable thrown = catchThrowable(() -> sut.handle(null));

        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }

    @Test
    public void thatSongFetcherIsCalledWithSearchTerm() {
        List<SongPlatformInformation> expectedList = singletonList(aSongPlatformInformation());
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(expectedList);
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand("searchTerm");

        sut.handle(command);

        verify(iTunesSongFetcher).fetchSongs(eq("searchTerm"));
        verify(randomNameGenerator, never()).generateName();
    }

    @Test
    public void thatSongFetcherIsCalledWithRandomSearchTerm() {
        List<SongPlatformInformation> expectedList = singletonList(aSongPlatformInformation());
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(expectedList);
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand();

        sut.handle(command);

        verify(iTunesSongFetcher).fetchSongs(eq("randomName"));
        verify(randomNameGenerator).generateName();
    }

    @Test
    public void thatSongListIsReturned() {
        List<SongPlatformInformation> expectedList = singletonList(aSongPlatformInformation());
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(expectedList);
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand();

        List<SongEntity> songs = sut.handle(command);

        verify(iTunesSongFetcher, atLeast(1)).fetchSongs(any());
        assertThat(songs).isNotEmpty();
    }

    @Test
    public void thatSongListIsStoredInDb() {
        List<SongPlatformInformation> expectedList = singletonList(aSongPlatformInformation());
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(expectedList);
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand();

        sut.handle(command);

        verify(songRepository).saveAll(any());
    }

    @Test
    public void thatRetriesAreMadeIfSearchTermIsRandom() {
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(emptyList());
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand();

        List<SongEntity> songs = sut.handle(command);

        verify(songRepository, never()).saveAll(any());
        verify(randomNameGenerator, times(21)).generateName();
        verify(iTunesSongFetcher, times(20)).fetchSongs(any());
        assertThat(songs).isEmpty();
    }

    @Test
    public void thatOnlyNecessaryRetriesAreMadeIfSearchTermIsRandom() {
        List<SongPlatformInformation> expectedList = singletonList(aSongPlatformInformation());
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(emptyList()).thenReturn(expectedList);
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand();

        List<SongEntity> songs = sut.handle(command);

        verify(songRepository).saveAll(any());
        verify(randomNameGenerator, times(2)).generateName();
        verify(iTunesSongFetcher, atLeast(2)).fetchSongs(any());
        assertThat(songs).isNotEmpty();
    }

    @Test
    public void thatEmptyListIsReturnedIfNoFetchersAreFound() {
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand("harry potter");

        sut = new FetchSongsFromRemoteCommandHandler(songRepository, emptyList(), randomNameGenerator);
        List<SongEntity> songs = sut.handle(command);

        verify(songRepository, never()).saveAll(any());
        assertThat(songs).isEmpty();
    }


    @Test
    public void thatMultipleSongFetchersAreQueriedAndResultsCombined() {
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(asList(aSongPlatformInformationWithType(ITUNES), aSongPlatformInformationWithType(ITUNES)));
        when(spotifyPlatformFetcher.fetchSongs(any())).thenReturn(asList(aSongPlatformInformationWithType(SPOTIFY), aSongPlatformInformationWithType(SPOTIFY)));
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand("harry potter");

        sut = new FetchSongsFromRemoteCommandHandler(songRepository, asList(iTunesSongFetcher, spotifyPlatformFetcher), randomNameGenerator);
        List<SongEntity> songs = sut.handle(command);

        verify(songRepository).saveAll(any());
        verify(iTunesSongFetcher, atLeast(1)).fetchSongs(any());
        verify(spotifyPlatformFetcher, atLeast(1)).fetchSongs(any());
        assertThat(songs).hasSize(2);
        assertThat(songs.get(0).getPlatformInformation()).hasSize(2);
    }

    @Test
    public void thatListContainsOnlyItemsWithResultsFromAllFetchers() {
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(asList(aSongPlatformInformationWithType(ITUNES), aSongPlatformInformationWithType(ITUNES)));
        when(spotifyPlatformFetcher.fetchSongs(any())).thenReturn(emptyList()).thenReturn(singletonList(aSongPlatformInformationWithType(SPOTIFY)));
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand("harry potter");

        sut = new FetchSongsFromRemoteCommandHandler(songRepository, asList(iTunesSongFetcher, spotifyPlatformFetcher), randomNameGenerator);
        List<SongEntity> songs = sut.handle(command);

        verify(songRepository).saveAll(any());
        verify(iTunesSongFetcher, atLeast(1)).fetchSongs(any());
        verify(spotifyPlatformFetcher, atLeast(1)).fetchSongs(any());
        assertThat(songs).hasSize(1);
        assertThat(songs.get(0).getPlatformInformation()).hasSize(2);
    }

    @Test
    public void thatEmptyListIsReturnedIfNotAllMatcherFindResults() {
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(asList(aSongPlatformInformationWithType(ITUNES), aSongPlatformInformationWithType(ITUNES)));
        when(spotifyPlatformFetcher.fetchSongs(any())).thenReturn(emptyList());
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand("harry potter");

        sut = new FetchSongsFromRemoteCommandHandler(songRepository, asList(iTunesSongFetcher, spotifyPlatformFetcher), randomNameGenerator);
        List<SongEntity> songs = sut.handle(command);

        verify(songRepository, never()).saveAll(any());
        verify(iTunesSongFetcher, atLeast(1)).fetchSongs(any());
        verify(spotifyPlatformFetcher, atLeast(1)).fetchSongs(any());
        assertThat(songs).isEmpty();
    }

    @Test
    public void thatEmptyListIsReturnedIfPrimarySongFetcherDoesNotDeliverResults() {
        when(iTunesSongFetcher.fetchSongs(any())).thenReturn(emptyList());
        when(spotifyPlatformFetcher.fetchSongs(any())).thenReturn(emptyList()).thenReturn(singletonList(aSongPlatformInformationWithType(SPOTIFY)));
        FetchSongsFromRemoteCommand command = new FetchSongsFromRemoteCommand("harry potter");

        sut = new FetchSongsFromRemoteCommandHandler(songRepository, asList(iTunesSongFetcher, spotifyPlatformFetcher), randomNameGenerator);
        List<SongEntity> songs = sut.handle(command);

        verify(songRepository, never()).saveAll(any());
        verify(iTunesSongFetcher, atLeast(1)).fetchSongs(any());
        verify(spotifyPlatformFetcher, never()).fetchSongs(any());
        assertThat(songs).isEmpty();
    }

    @SuppressWarnings("SpellCheckingInspection")
    private static class SpotifyPlatformFetcher implements SongFetcher {

        @Override
        public List<SongPlatformInformation> fetchSongs(String searchTerm) {
            return singletonList(aSongPlatformInformationWithType(SPOTIFY));
        }
    }
}