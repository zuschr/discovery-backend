package de.ebertsoftware.discovery;

import de.ebertsoftware.discovery.domain.song.PlatformType;
import de.ebertsoftware.discovery.domain.song.SongEntity;
import de.ebertsoftware.discovery.domain.song.SongPlatformInformation;

import java.util.UUID;

import static de.ebertsoftware.discovery.domain.song.PlatformType.ITUNES;
import static java.util.Collections.singletonList;

public class SongEntityFixtures {

    public static SongEntity aSongEntity() {
        return new SongEntity(UUID.randomUUID().toString(), singletonList(
                aSongPlatformInformation()
        ));
    }

    public static SongPlatformInformation aSongPlatformInformation() {
        return aSongPlatformInformationWithType(ITUNES);
    }

    public static SongPlatformInformation aSongPlatformInformationWithType(PlatformType platformType) {
        return new SongPlatformInformation(
                platformType,
                "songId",
                "artistId",
                "songName",
                "artistName",
                "previewUrl",
                "artworkUrl",
                singletonList("Classical")
        );
    }
}
