package de.ebertsoftware.discovery;

import com.mongodb.client.MongoDatabase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext
public class EmbeddedMongoTest {

    @Autowired
    MongoTemplate mongoTemplate;

    @Before
    public void setUp() { }

    @Test
    public void thatEmbeddedMongoDBIsUsed() {

        MongoDatabase mongoDatabase = mongoTemplate.getDb();

        assertThat(mongoDatabase.getName()).isEqualTo("embeded_db");
    }
}
